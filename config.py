import logging
import os


class Config:
    """ Base config. """
    DEBUG = False
    NEO4J_URI = os.getenv('NEO4J_URI', "localhost:7474")
    NEO4J_USERNAME = os.getenv('NEO4J_USERNAME', "neo4j")
    NEO4J_PASSWORD = os.getenv('NEO4J_PASSWORD', "password")
    SECRET_KEY = os.getenv('SECRET_KEY', 'change_me')
    JWT_AUTH_USERNAME_KEY = 'email'
    LOG_LEVEL = os.getenv('LOG_LEVEL', logging.DEBUG)


class DevelopmentConfig(Config):
    """ Development configuration. """
    DEBUG = True


class TestingConfig(Config):
    """ Testing configuration used in unit tests. """
    DEBUG = True
    TESTING = True


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
}
