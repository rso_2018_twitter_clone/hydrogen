# Oxygen - user-related API Service

## Project contents

### Code

* `config.py` - global configuration file
* `run.py` - Python script starting app in development mode
* `hydrogen/` - main application package
* `hydrogen/__init__.py` - initializes flask application
* `hydrogen/views.py` - API endpoints definitions
* `hydrogen/*.py` - modules used by app
* `tests/test_*.py` - test modules - one per app module

### Development tools

* `check_style.sh` - helper script used to run code quality tools (flake8, isort)
* `init_hooks.sh` - helper script creating necessary git hook symlink
* `requirements.txt` - pip requirements list
* `setup.cfg` - config used by various Python tools (flake8, isort)

## Setup

1. Create virtualenv with python3:

   ```
   $ mkvirtualenv -ppython3 -a `pwd` -r requirements.txt hydrogen
   ```

   After that newly created virtualenv should be activated which is denoted by
   `(hydrogen)` prefix in terminal prompt.

   ```
   (hydrogen) user@hostame:~/workspace/hydrogen$
   ```

2. Install hooks:

   ```
   (hydrogen)$ ./init_hooks.sh
   ```

## Adding a dependency

To add new dependency simply install it with `pip` with virtualenv activated.

```sh
(hydrogen)$ pip install somelib
```

Then you should regenerate `requrements.txt` by executing:

```
(hydrogen)$ pip freeze > requirements.txt
```

## Tests

To run test suite execute the following:

```
(hydrogen)$ ./manage.py test
```

## Running

### Development

Simply run the `manage.py runserver` script with virtualenv activated:

```
(hydrogen)$ ./manage.py runserver
# or
(hydrogen)$ python manage.py runserver
```

### Docker

If you haven't used giltab registry before you need to log in:

```
$ docker login registry.gitlab.com
```

Do run latest version of hydrogen container, simply run:

```
$ docker run registry.gitlab.com/rso_2018_twitter_clone/hydrogen:latest
```

## CI

Continous Integration pipeline is divided into following stages:

1. Build
2. Test
3. Release

### Build

This stage pulls docker image from `python:alpine` image and installs python
requirements. Fresh container is pushed to gitlab registry with tag `hydrogen:<refname>`.

### Test

In this stage image built in previous step is pulled and then test scripts are run
inside the container. This ensures that the code passes tests on target environment.
Note that the image entrypoint is `'python'` and shell is not installed so
test scripts are run as python modules.

### Release

This stage is run only on master. If an image passes tests, then it is tagged
as `hydrogen:latest` and then pushed back into registry.
