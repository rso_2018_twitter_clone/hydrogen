#!/usr/bin/env python
import sys
import unittest

from flask_script import Command, Manager, Server

from hydrogen import app, socketio


class TestCommand(Command):
    """ Runs tests using unittest. """
    def run(self):
        test_loader = unittest.defaultTestLoader
        test_runner = unittest.TextTestRunner()
        test_suite = test_loader.discover('.')
        test_runner.run(test_suite)


class RunCommand(Server):
    """ Runs tests using unittest. """
    def run(self):
        test_loader = unittest.defaultTestLoader
        test_runner = unittest.TextTestRunner()
        test_suite = test_loader.discover('.')
        test_runner.run(test_suite)

    def __call__(self, app, host, port, use_debugger, use_reloader, threaded, processes, passthrough_errors, ssl_crt, ssl_key):
        # we don't need to run the server in request context
        # so just run it directly

        if use_debugger is None:
            use_debugger = app.debug
            if use_debugger is None:
                use_debugger = True
                if sys.stderr.isatty():
                    print("Debugging is on. DANGER: Do not allow random users to connect to this server.", file=sys.stderr)
        if use_reloader is None:
            use_reloader = use_debugger

        socketio.run(
            app,
            host=host,
            port=port,
            debug=use_debugger,
            use_reloader=use_reloader,
            **self.server_options,
        )


manager = Manager(app)
manager.add_command('test', TestCommand)
manager.add_command('runserver', RunCommand)


if __name__ == '__main__':
    manager.run()
