class Clients():
    def __init__(self):
        self.clients = {}

    def add(self, uid, sid):
        self.clients = {**self.clients, uid: sid}

    def get(self, uid):
        return self.clients.get(uid)

    def remove(self, sid):
        self.clients = {u: s for (u, s) in self.clients.items() if s != sid}
