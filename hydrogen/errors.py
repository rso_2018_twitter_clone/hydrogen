from flask import jsonify, request

from hydrogen import app


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'error': 'Not found {0}'.format(request.path),
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp
