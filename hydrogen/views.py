import logging

from flask import abort, jsonify, request
from flask_jwt import current_identity, jwt_required
from flask_socketio import disconnect, emit, join_room, leave_room, send
import jwt
from py2neo import Node, NodeSelector, Relationship

from hydrogen import app, graph, socketio
from hydrogen.clients import Clients


def respond_error(code, data):
    response = jsonify(data)
    response.status_code = code
    abort(response)


@app.route('/health')
def health():
    return jsonify({"alive": True})


clients = Clients()


@socketio.on('login')
def on_login(data):
    try:
        logging.debug('got data %s', data)
        payload = jwt.decode(data['access_token'], app.config['SECRET_KEY'])

        sid = request.sid
        join_room(sid)
        clients.add(payload['identity'], sid)
        send(sid + ' has entered the room.', room=sid)
        logging.debug('new client, clients: %s', clients.clients)
    except jwt.exceptions.DecodeError:
        disconnect()
        return


@socketio.on('disconnect')
def on_leave():
    sid = request.sid
    leave_room(sid)
    send(sid + ' has left the room.', room=sid)
    logging.debug('%s has left the room.', sid)
    clients.remove(sid)


@app.route('/new_post', methods=['POST'])
def new_post():
    data = request.json
    if not {'user_id', 'post_id'}.issubset(data):
        abort(400)

    user_id = data['user_id']
    user = NodeSelector(graph).select("Person", user_id=user_id).first()

    followers = [rel.start_node()["user_id"] for rel in graph.match(end_node=user, rel_type="SUBSCRIBED")]
    logging.debug('new post from %d to %s', user_id, followers)
    logging.debug('clients %s', clients.clients)
    for follower_id in followers:
        sid = clients.get(follower_id)
        if sid:
            emit('post', data, room=sid, namespace='/')

    return jsonify({"status": "ok"})


@app.route('/subscribe', methods=["POST"])
@jwt_required()
def subscribe():
    data = request.json
    if not {'subscribed'}.issubset(data):
        abort(400)

    user = NodeSelector(graph).select("Person", user_id=int(current_identity)).first()
    # user = NodeSelector(graph).select("Person", user_id=int(request.args.get('user'))).first()
    if user is None:
        user = Node("Person", user_id=int(current_identity))
        # user = Node("Person", user_id=int(request.args.get('user')))
        graph.create(user)

    subscribed_id = data.get('subscribed')

    subscribed = NodeSelector(graph).select(
        "Person", user_id=int(subscribed_id)).first()

    if subscribed is None:
        subscribed = Node("Person", user_id=int(subscribed_id))
        graph.create(subscribed)

    relation = Relationship(user, "SUBSCRIBED", subscribed)
    graph.create(relation)

    return jsonify({"message": "subsribction success"})


@app.route('/subscribe/<int:user_id>', methods=["DELETE"])
@jwt_required()
def unsubscribe(user_id):
    user = NodeSelector(graph).select("Person", user_id=int(current_identity)).first()
    to_unsubscribe = NodeSelector(graph).select("Person", user_id=int(user_id)).first()

    if user is None or to_unsubscribe is None:
        return jsonify({"message": "subscription do not exist"})

    relation = graph.match_one(start_node=user, rel_type="SUBSCRIBED", end_node=to_unsubscribe)

    if relation is not None:
        graph.separate(relation)

    return jsonify({"message": "unssubscribe success"})


@app.route('/followers/<int:user_id>', methods=["GET"])
def get_followers(user_id):
    user = NodeSelector(graph).select("Person", user_id=user_id).first()
    if user is None:
        return jsonify({"followers": []})

    followers = [rel.start_node()["user_id"] for rel in graph.match(end_node=user, rel_type="SUBSCRIBED")]

    return jsonify({"followers": followers})


@app.route('/subscriptions/<int:user_id>', methods=["GET"])
def get_subscriptions(user_id):
    user = NodeSelector(graph).select("Person", user_id=user_id).first()
    if user is None:
        return jsonify({"subscriptions": []})

    subscriptions = [rel.end_node()["user_id"] for rel in graph.match(start_node=user, rel_type="SUBSCRIBED")]

    return jsonify({"subscriptions": subscriptions})


@app.route('/my_distance/<int:watched_id>', methods=["GET"])
@jwt_required()
def my_distance(watched_id):
    user_id = int(current_identity)
    # user_id = int(request.args.get('user'))
    result = graph.data(
        "MATCH p=shortestPath((a:Person {{user_id: {0} }})-[*..5]-(b:Person {{user_id: {1} }})) RETURN length(p)".format(
            user_id, watched_id
        )
    )

    if result:
        result = result[-1]['length(p)']
    else:
        result = None

    return jsonify({"path_length": result})
