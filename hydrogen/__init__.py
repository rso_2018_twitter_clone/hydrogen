import logging

from flask import Flask
from flask_socketio import SocketIO
from flask_jwt import JWT
from flask_cors import CORS
from py2neo import authenticate as neoauth, Graph

from config import app_config


app = Flask(__name__)
app.config.from_object(app_config['development'])
logging.getLogger().setLevel(app.config['LOG_LEVEL'])
CORS(app)

uri, user, password = app.config['NEO4J_URI'], app.config['NEO4J_USERNAME'], app.config['NEO4J_PASSWORD']
neoauth(uri, user, password)
graph = Graph('http://{0}/db/data'.format(uri))


def authenticate(username, password):
    pass


def identity(payload):
    return payload['identity']


jwt = JWT(app, authenticate, identity)

socketio = SocketIO(async_mode='eventlet')
socketio.init_app(app)

import hydrogen.errors  # NOQA
import hydrogen.views  # NOQA
