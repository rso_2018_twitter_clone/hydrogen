#!/bin/bash

set -e

cmd="$@"

if [[ ! -z "${NEO4J_URI}" ]]; then
    end="$((SECONDS+60))"
    while true; do
        [[ "200" = "$(curl --silent --write-out %{http_code} --output /dev/null http://$NEO4J_URI)" ]] && break
        >&2 echo "neo4j is down (http://$NEO4J_URI), waiting..."
        [[ "${SECONDS}" -ge "${end}" ]] && exit 1
        sleep 1
    done
fi

>&2 echo "neo4j is up - executing command"
python $cmd

