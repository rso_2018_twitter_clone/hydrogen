FROM python:3.6

MAINTAINER Michał Błotniak "buoto336@gmail.com"

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f http://localhost:5000/health || exit 1

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["manage.py", "runserver", "--host=0.0.0.0"]
